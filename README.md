# Neochat

Neochat is a client for Matrix, the decentralized communication protocol for instant
messaging. It is a fork of Spectral, using KDE frameworks.

## Contact

You can reach the maintainers at #neochat:kde.org, if you are already on Matrix.
Development happens in http://invent.kde.org/network/neochat (not in GitHub).

## Acknowledgement

This program utilizes [libQuotient](https://github.com/quotient-im/libQuotient/) library and some C++ models from [Quaternion](https://github.com/quotient-im/Quaternion/).

This program is a fork of [Spectral](https://gitlab.com/spectral-im/spectral/).

## License

![GPLv3](https://www.gnu.org/graphics/gplv3-127x51.png)

This program is licensed under GNU General Public License, Version 3. 

